<?php

include('connect.php');



$item_per_page = 3;

//$page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

$page_number = filter_var($_GET["page"]);

if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit();
}

$position = (($page_number-1) * $item_per_page);


$tag = $_GET['tag'];
//$tag = 'start';

if(is_numeric($tag)){		
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale
		FROM products
		WHERE visible=1
		AND start = 1
		AND cat_id=?
		ORDER BY id DESC LIMIT ?, ?
		
		");	
		$results->bind_param("ddd", $tag, $position, $item_per_page); 

}



if(!is_numeric($tag)){
	    
	if($tag == "start"){
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale
		FROM products
		WHERE visible=1
		AND start=1
		ORDER BY id DESC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
		
	}
	
	if($tag == "pop"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale
		FROM products
		WHERE visible=1
		AND pop=1
		ORDER BY id DESC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "accessory"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale
		FROM products
		WHERE visible=1
		AND accessory=1
		ORDER BY id DESC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "allfeed"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale
		FROM products
		WHERE visible=1
		ORDER BY id DESC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
		    

}



$results->execute(); //Execute prepared Query
$results->bind_result($pid, $p_name, $p_desc, $p_images, $price_out, $price_sale); //bind variables to prepared statement





//output results from database



$n = 0;
while($results->fetch()){ //fetch values
	
	
	
	 ?>
	 <div class="box_mobile">
	  <div class="one_prod">
		 <div class="mob_hero" style="background-image: url(<?php echo 'http://gifamin.com/images/products_pics/'.$pid.'/1.jpg';?>);" onclick="oneProd(<?php echo $pid?>);"></div>
		 
		
		 
		 <div class="prod_data">
							<div class="inbox bt">
								<div class="prod_title inl_m">
									<div class="elips"></div>
									<?php echo $p_name;?>
								</div>
								<div class="prod_price inl_m">
									<div class="inprice">
										<?php 
											if($price_sale>0){
												$showprice = $price_sale;
											}else{
												$showprice = $price_out;
											}
											
											echo number_format($showprice, 0, '.', ' ');
											
										?> <span>грн.</span></div>
								</div>
							</div>
						</div>
						
		 
	  </div>
	 </div>
	 
	<?php
	}

?>

