<link rel="apple-touch-icon" sizes="180x180" href="/images/favx/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favx/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favx/favicon-16x16.png">
<link rel="manifest" href="/images/favx/site.webmanifest">
<link rel="mask-icon" href="/images/favx/safari-pinned-tab.svg" color="#d62148">
<link rel="shortcut icon" href="/images/favx/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Gifamin">
<meta name="application-name" content="Gifamin">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="/images/favx/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<!--
<link rel="apple-touch-icon" sizes="180x180" href="/images/favi/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favi/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favi/favicon-16x16.png">
<link rel="manifest" href="/images/favi/site.webmanifest">
<link rel="mask-icon" href="/images/favi/safari-pinned-tab.svg" color="#282b48">
<link rel="shortcut icon" href="/images/favi/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Gifamin">
<meta name="application-name" content="Gifamin">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="/images/favi/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
-->
<!--
<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
<link rel="manifest" href="images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
-->
<!--
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/favicomatic/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/favicomatic/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/favicomatic/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/favicomatic/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="images/favicomatic/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="images/favicomatic/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="images/favicomatic/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="images/favicomatic/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="images/favicomatic/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="images/favicomatic/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="images/favicomatic/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="images/favicomatic/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="images/favicomatic/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
-->
<!--
<link rel="apple-touch-icon" sizes="180x180" href="images/favreal/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favreal/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favreal/favicon-16x16.png">
<link rel="manifest" href="images/favreal/site.webmanifest">
<link rel="mask-icon" href="images/favreal/safari-pinned-tab.svg" color="#4766b0">
<link rel="shortcut icon" href="images/favreal/favicon.ico">
<meta name="apple-mobile-web-app-title" content="Gifamin">
<meta name="application-name" content="Gifamin">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-config" content="images/favreal/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
-->