<?php

include('connect.php');



$item_per_page = 6;

//$page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

$page_number = filter_var($_GET["page"]);

if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit();
}

$position = (($page_number-1) * $item_per_page);


$tag = $_GET['tag'];
//$tag = 'start';

if(is_numeric($tag)){		
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND start = 1
		AND cat_id=?
		ORDER BY priority ASC LIMIT ?, ?
		
		");	
		$results->bind_param("ddd", $tag, $position, $item_per_page); 

}



if(!is_numeric($tag)){
	    
	if($tag == "start"){
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND start=1
		ORDER BY priority ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
		
	}
	
	if($tag == "popular"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND popular=1
		ORDER BY id ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	
	
	//TAGS
	
	if($tag == "tag_a"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_a=1
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
    if($tag == "tag_b"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_b=1
		ORDER BY priority ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_c"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_c=1
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_d"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_d=1
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_e"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND price_sale!=0
		ORDER BY id ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "tag_event"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND tag_event=1
		ORDER BY priority ASC LIMIT ?, ?
		");
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	
	//TAGS
	
	
	
	if($tag == "accessory"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		AND accessory=1
		ORDER BY priority ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
	
	if($tag == "allfeed"){
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale, products.pack_type, products.priority, products.author, products.cat_id, p_cats.name
		FROM products
		
		LEFT JOIN p_cats 
        ON (products.cat_id=p_cats.id)
		
		WHERE visible=1
		ORDER BY priority ASC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
	}
		    

}



$results->execute(); //Execute prepared Query
$results->bind_result($pid, $p_name, $p_desc, $p_images, $price_out, $price_sale, $pack_type, $priority, $author, $cat_id, $cat_name); //bind variables to prepared statement





//output results from database



$n = 0;
while($results->fetch()){ //fetch values
	
	
	
	 ?>
	 <div class="box_mobile inl_t" style="width:368px; margin: 0 10px 25px 15px;">
	  <div class="one_prod inl_t" style="width: 100%;">
		 <div class="mob_hero" style="background-image: url(<?php echo 'http://gifamin.com/images/products_pics/'.$pid.'/1.jpg';?>); " onclick="oneProd(<?php echo $pid?>);">
			 
			 <?php if($price_sale>0){
				 //if($discount>0){
				  $oldprice = number_format($price_out, 0, '.', ' ');
				  $newprice = number_format($price_sale, 0, '.', ' ');
				  $discount = number_format( (100-(100*$price_sale/$price_out)), 0, '.', ' ');
				  echo "<div class='discount_bage'>
				  <div class='bage_nums'>
				  	<div class='bage_old_price inl_m'>$price_out <span>грн</span></div>
				  	<div class='bage_dis inl_m'>-$discount<span>%</span></div>
				  </div>
				  </div>";
				  //}
			  }?>
			 
		 </div>
		 
		
		 
		 <div class="prod_data" style="width: 100%;">
							<div class="inbox bt">
								<div class="prod_title inl_m" onclick="oneProd(<?php echo $pid?>);">
									<div class="elips"></div>
									<?php echo 'Подарочный набор '.html_entity_decode($p_name);?>
								</div>
								<div class="prod_price inl_m">
									<div class="inprice" onclick="oneProd(<?php echo $pid?>);">
										<?php 
											if($price_sale>0){
												$showprice = $price_sale;
											}else{
												$showprice = $price_out;
											}
											
											echo number_format($showprice, 0, '.', ' ');
											
										?> <span>грн.</span></div>
								</div>
							</div>
						</div>
		
		 <!-- -->
			<div class="inbox bt" style="font-size: 0;">
							
							
							<?php if($author != 0){
								
								
								
								if($author == 1){
									$a_sign = 'bach.png';
									$a_text = 'Рекомендовано Иваном Бачуриным, лучшим сомелье Украины 2009, бренд-амбассадором Moёt Hennessy в странах СНГ';
								}
								
								else if($author == 2){
									$a_sign = 'ksen.png';
									$a_text = 'Рекомендовано Виктором Ксенофонтовым, сомелье ресторана "Велюр"';
								}
								
								else if($author == 3){
									$a_sign = 'platon.png';
									$a_text = '';
								}
								else if($author == 4){
									$a_sign = 'alex.png';
									$a_text = 'Рекомендовано Александром Лавриненко, победителем конкурса «Украинский сомелье 2014», лучшим сомелье Украины 2015';
								 }
								
								
								?>
								
								
								
								
								
								
								<div class="front_signature inl_m" style="border:none; width:100%;">
								<div class="front_signature_text inl_m"><?php echo $a_text;?></div>
								<div class="front_signature_pic inl_m" style="background-image: url(../images/signs/<?php echo $a_sign?>);"></div>
							</div> 
							<div class="share_this inl_m"><!--share_this to share_this_without-->
								<?php
							}else{?>
							
							    <?php $a_text = 'Рекомендовано командой экспертов «Gifamin»'; ?>
							
							    <div class="front_signature inl_m" style="border:none; width:100%;">
								<div class="front_signature_text_small inl_m"><?php echo $a_text;?></div>
								<div class="front_signature_pic_small inl_m"></div>
							</div> 
							
							
							
							
							<div class="share_this inl_m">
							
							
								<!-- <div class="share_this_without inl_m">  -->
								
							<?php	
							}?>
							
								
								<div class="stats_data" attr-index="<?php echo $n?>" attr-name="<?php echo html_entity_decode($p_name);?>" attr-id="<?php echo $pid?>" attr-cat="<?php echo $cat_name?>" attr-price="<?php echo $showprice?>"></div>
								
								
							</div>
						
						
						</div>
		 
	  </div>
		<!-- -->	
						
		 
	  </div>
	 </div>
	 
	<?php
	}

?>

